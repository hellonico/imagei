(ns fstego.core
 (:import [javax.imageio ImageIO])
 (:import [java.io File])
)

(defn read-image [ path ]
   (ImageIO/read (File. path)))

(defn write-image [ src new-path]
(ImageIO/write
 src
 "PNG"
 (clojure.java.io/as-file new-path)))

(defn new-filter [ class-string]
  (let [
        constructor
        (first
         (.getDeclaredConstructors (load-string  (str "com.jhlabs.image." class-string))))]
  (.newInstance
    constructor
    (into-array Object [] ))))

(defn apply-filter [ filter-class img]
  (.filter
   (new-filter filter-class) img nil))

(defn debug-image [img]
 (println
  "Input Image has the following dimensions: "
  (.getWidth img) "x" (.getHeight img)))

(def img1
  (read-image
   "/Users/niko/Dropbox/mds/paper/factorial.png"))
(write-image
 (apply-filter "JavaLnFFilter" img1)
 "grey.png")


(defn -main[& args]
(if-let [ path (first args) ]
(let[ img (read-image path) ]
 (debug-image img)
(println "No image")
)))
